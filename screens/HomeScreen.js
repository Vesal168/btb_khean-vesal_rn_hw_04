
import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  FlatList,
} from 'react-native';

const img = 'http://books.google.com/books/content?id=PCDengEACAAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api';

function HomeScreen({navigation}) {
  const [data, setData] = useState([
    {
      key: '1',
      name:
        'Huawei បាន​ប្រកាស​ចេញ​ស្មាតហ្វូន Nova 8 SE ជា​ផ្លូវ​ការ ក្រោយ​ពី​ nova 7 SE បាន​ប្រកាស​ចេញ​ទើប​ជាង ៦​ខែ​ប៉ុណ្ណោះ។',
      email: 'miyah.myles@gmail.com',
      date: '2020-11-02',
      photo:
        'https://i1.wp.com/celebmix.com/wp-content/uploads/2018/12/blackpink-a-year-in-review-01.jpg?fit=758%2C379&ssl=1',
    },
    {
      key: '2',
      name: 'iPhone 12 និង 12 Pro មានលក់មួយទឹកតម្លៃប៉ុន្មានពេលនេះ',
      email: 'june.cha@gmail.com',
      date: '2020-11-02',
      photo:
        'https://cdn.sabay.com/cdn/media.sabay.com/media/TECH-KK/MENGNGY/Event(1)/iPhone-12/5f9678ccee8e6_1603696800_medium.jpg',
    },
    {
      key: '3',
      name: 'Huawei Navi 3N បាន​ប្រកាស​ចេញ​ស្មាតហ្វូន nova 8 SE ជា​ផ្លូវ​ការ ក្រោយ​ពី​ nova 7 SE បាន​ប្រកាស​ចេញ​ទើប​ជាង ៦​ខែ​ប៉ុណ្ណោះ។',
      email: 'june.cha@gmail.com',
      date: '2020-11-04',
      photo:
        'https://cdn.sabay.com/cdn/media.sabay.com/media/TECH-KK/MENGNGY/Event(1)/iPhone-12/5f9678ccee8e6_1603696800_medium.jpg',
    },
    {
      key: '4',
      name: 'ក្រុមហ៊ុន Toyota បានប្រកាសចេញលក់រថយន្ត Crown ស៊េរីឆ្នាំ២០២១ ជាផ្លូវការ សម្រាប់ទីផ្សារប្រទេសជប៉ុន។',
      email: 'june.cha@gmail.com',
      date: '2020-11-08',
      photo:
        'https://cdn.sabay.com/cdn/media.sabay.com/media/sabay-news/Technology-News/Long-Tech/F4/5fa365a481c0b_1604543880_medium.jpg',
    },
    {
      key: '5',
      name: 'ខ្សែ​បម្រើ​ក្លឹប​ Tottenham Hotspur កីឡាករ​ Giovani Lo Celso បាន​ស៊ុត​ចូល​គ្រាប់​បាល់​​ ១​គ្រាប់​ក្នុង​ការ​តទល់​នឹង​ក្លឹប​ Ludogorets នៅ​ក្របខ័ណ្ឌ​ Europa League ក្នុងថ្ងៃនេះតែ​បែរ​ជា​ត្រូវ​បាន​លោក​ Jose Mourinho ស្ដី​បន្ទោស​ទោ​វិញ​។z',
      email: 'june.cha@gmail.com',
      date: '2020-11-09',
      photo:
        'https://cdn.sabay.com/cdn/media.sabay.com/media/sabay-news/Sport-News/International-Sports/Freelancer-Sport/football(38)/5fa5510974ff4_1604669700_medium.jpg',
    },
  ]);
  return (
    <View style={styles.container}>
      <View>
        <Text style={{fontWeight: '500',fontSize: 18, marginLeft: 15, marginTop: 10, bottom: 8}}>
          Your Daily Read
        </Text>
       
      </View>
      <FlatList
        data={data}
        renderItem={({item}) => (
          <TouchableOpacity
            onPress={() => navigation.navigate('DetailsScreen', item)}>
            <View style={styles.listItem}>
              <Image
                source={{uri: item.photo}}
                style={{width: 80, height: 70, borderRadius: 5}}
              />
              <View style={{alignItems: 'center', flex: 1}}>
                <Text
                  style={{fontWeight: 'bold', marginLeft: 10}}
                  numberOfLines={2}
                  ellipsizeMode={'tail'}>
                  {item.name}
                </Text>
                <Text style={{fontSize: 11, marginRight: 145, marginTop: 4}}>
                  {item.date}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        )}
        keyExtractor={item => item.key}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F7F7F7',
    marginTop: 15,
  },
  listItem: {
    margin: 10,
    padding: 10,
    backgroundColor: '#FFF',
    width: '90%',
    flex: 1,
    alignSelf: 'center',
    flexDirection: 'row',
    borderRadius: 5,
  },
  cardList: {
    margin: 10,
    bottom: 2,
    alignItems: 'center',
  },
});

export default HomeScreen;
