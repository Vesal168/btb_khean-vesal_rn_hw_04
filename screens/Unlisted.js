


import React from 'react';
import {View, Image} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Title,
  Text,
  CardItem,
} from 'native-base';
import {Linking} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

function Unlisted({navigation, route}) {
  const {name, date, photo} = route.params;
  return (
    <Container>
      <View>
        <Container>
          <Header>
            <Left>
              <Button transparent>
                <Text onPress={() => navigation.goBack()} style={{fontSize: 12}}>Back</Text>
              </Button>
            </Left>
            <Body>
              <Title style={{marginLeft: 40}}>Detail Screen</Title>
            </Body>
            <Right>
              <Button transparent>
                <Text>Cancel</Text>
              </Button>
            </Right>
          </Header>
        </Container>
      </View>
      <View style={{marginTop: 80, padding: 10}}>
        <Text numberOfLines={2} ellipsizeMode="tail">
          {name}
        </Text>
        <Text style={{fontSize: 10}}>{date}</Text>
        <CardItem cardBody>
          <Image
            source={{uri: photo}}
            style={{
              backgroundColor: '#eee',
              marginTop: 30,
              height: 200,
              width: null,
              flex: 1,
            }}
          />
        </CardItem>
        <View style={{marginTop: 40, marginLeft: 90, flexDirection: 'row'}}>
          <Icon.Button
            name="logo-facebook"
            onPress={() => {
              Linking.openURL('https://www.facebook.com/');
            }}
            size={40}
            backgroundColor="#fff"
            color="orange"
          />
          <Icon.Button
            name="logo-google"
            onPress={() => {
              Linking.openURL('https://www.google.com/');
            }}
            size={40}
            backgroundColor="#fff"
            color="orange"
          />
          <Icon.Button
            name="logo-youtube"
            onPress={() => {
              Linking.openURL('https://www.youtube.com/');
            }}
            size={40}
            backgroundColor="#fff"
            color="orange"
          />
        </View>
      </View>
    </Container>
  );
}
export default Unlisted;
