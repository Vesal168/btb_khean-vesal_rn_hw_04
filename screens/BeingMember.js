import React from 'react';
import {View, StyleSheet} from 'react-native';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Title,
  Card,
  CardItem,
  Text
} from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';
import {ScrollView} from 'react-native';

const BeingMember = ({navigation}) => {
  return (
    <Container>
      <Header>
        <Left>
          <Button transparent>
            <Text onPress={() => navigation.goBack()} style={{fontSize:12}}>Back</Text>
          </Button>
        </Left>
        <Body>
          <Title>Member</Title>
        </Body>
        <Right>
          <Button transparent>
            <Text>Cancel</Text>
          </Button>
        </Right>
      </Header>
      <ScrollView>
        <Body>
          <View style={styles.title}>
            <Text style={styles.titleText}>Unlimited Reading.</Text>
            <Text style={styles.titleText}>Free for 1 Month.</Text>
          </View>
          <View style={styles.content}>
            <Text style={styles.contentText}>
              Read on any device, ads-free, and offline. We'll remind you 3 days
              before you trial ends. Cancel anytime.
            </Text>
          </View>
        </Body>
        <View style={{marginTop: 20}}>
          <Container>
            <Card style={{width: '90%', marginLeft: 20}}>
              <CardItem>
                <Body>
                  <Text
                    style={{marginLeft: 120, fontSize: 20, fontWeight: '600'}}>
                    $5/month
                  </Text>
                  <Text style={{marginLeft: 100, fontSize: 18, marginTop: 8}}>
                    First month free
                  </Text>
                  <View style={{fontSize: 18, marginTop: 8}}>
                    <Button dark style={{width: 308}}>
                      <Text
                        style={{
                          color: '#fff',
                          textAlign: 'center',
                          marginLeft: 100,
                        }}>
                        {' '}
                        Start your free trial{' '}
                      </Text>
                    </Button>
                  </View>
                </Body>
              </CardItem>
            </Card>
            <Body style={{marginTop: 40}}>
              <Text style={{marginLeft: 20, fontSize: 20, fontWeight: '600'}}>
                $5/month
              </Text>
              <Text style={{marginLeft: 20, fontSize: 18, marginTop: 8}}>
                First month free
              </Text>
              <View style={{fontSize: 18, marginTop: 8}}>
                <Button style={{width: 340, backgroundColor: '#fff', borderWidth: 1}}>
                  <Text
                    style={{
                      color: '#000',
                      textAlign: 'center',
                      marginLeft: 120,
                    }}>
                    Start your free trial
                  </Text>
                </Button>
              </View>
              <View style={{marginTop: 50}}>
                <Text style={{fontSize: 20, fontWeight: '600', marginRight: 200}}>Term & Conditions</Text>
              </View>
              <View>
                <Text style={{fontSize: 16, textAlign: 'justify' , padding: 15,marginTop: 15, lineHeight: 25}}>
                  Thanks for using Medium. Our mission is to deepen people's understanding of the world and spread ideas tjat matter.is made from effective building blocks referred to as components. The Components are constructed in pure React Native platform along with some JavaScript functionality with rich set of customisable properties.These components allow you to quickly build the perfect interface, includes components such as anatomy of your app screens
                </Text>
              </View>
            </Body>
          </Container>
        </View>
      </ScrollView>
    </Container>
  );
};

export default BeingMember;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    marginTop: 15,
    textAlign: 'center',
  },
  titleText: {
    fontSize: 24,
    fontWeight: '600',

  },
  content: {
    marginTop: 20,
  },
  contentText: {
    justifyContent: 'center',
    fontSize: 16,
    padding: 15
  },
});
