
import React, {Component} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Image,
  StyleSheet,
  TouchableOpacity,
  FlatList
} from 'react-native';
import {Container, ActionSheet,Header, Left, Body, Right, Button, Title} from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons';

var BUTTONS = ["Option 0", "Option 1", "Option 2", "Delete", "Cancel"];
var DESTRUCTIVE_INDEX = 3;
var CANCEL_INDEX = 4;

class Draft extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  delete = () => {
    ActionSheet.show(
        {
          options: BUTTONS,
          cancelButtonIndex: CANCEL_INDEX,
          destructiveButtonIndex: DESTRUCTIVE_INDEX,
          title: "Testing ActionSheet"
        },
        buttonIndex => {
          this.setState({ clicked: BUTTONS[buttonIndex] });
        }
      )
  }

  render() {
    const {navigation} = this.props;
    const {route} = this.props;
    const {des, resourcePath} = route.params;
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Text
                onPress={() => navigation.goBack()}
                style={{color: 'white'}}>
                Back
              </Text>
            </Button>
          </Left>
          <Body>
            <Title style={{marginLeft: 100}}>Draft</Title>
          </Body>
          <Right>
            <Button transparent>
              <Text style={{color: 'white'}}>Cancel</Text>
            </Button>
          </Right>
        </Header>

        <View style={styles.container}>
          <View style={styles.listItem}>
            <Image
              source={{uri: resourcePath}}
              style={{width: 60, height: 60, borderRadius: 30}}
            />
            <View style={{alignItems: 'center', flex: 1, marginTop: 10}}>
              <Text style={{fontWeight: 'bold', fontSize: 18}}>{des}</Text>
              <Text style={{fontSize: 11}}>11-11-2020</Text>
            </View>
            <TouchableOpacity
              style={{
                height: 35,
                width: 60,
                marginTop: 15,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'pink',
                borderRadius: 5,
              }}>
              <Text style={{color: '#000'}}>More</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Container>
    );
  }
}

export default Draft;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F7F7F7',
    marginTop: 15,
  },
  listItem: {
    margin: 10,
    padding: 10,
    backgroundColor: '#FFF',
    width: '80%',
    flex: 1,
    alignSelf: 'center',
    flexDirection: 'row',
    borderRadius: 5,
  },
});
