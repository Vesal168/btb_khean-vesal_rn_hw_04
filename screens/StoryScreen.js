import React from 'react';
import {View, StyleSheet, Image} from 'react-native';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title,
  Text,
  Content,
  Tab,
  Tabs,
} from 'native-base';
import ProfileScreen from './ProfileScreen';

const StoryScreen = ({navigation, route}) => {

  return (
    <Container>
      <Header>
        <Left>
          <Button transparent>
            <Text onPress={() => navigation.goBack()} style={{fontSize: 12}}>Back</Text>
          </Button>
        </Left>
        <Body>
          <Title>Story Screen</Title>
        </Body>
        <Right>
          <Button transparent>
            <Text>Cancel</Text>
          </Button>
        </Right>
      </Header>
      <Container>
        <Tabs>
          <Tab heading="Drafts">
            <ProfileScreen/>
          </Tab>
          <Tab heading="Public" />
          <Tab heading="Unlisted">
            
          </Tab>
        </Tabs>
      </Container>
    </Container>
  );
};

export default StoryScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container1: {
    flex: 1,
    backgroundColor: '#F7F7F7',
    marginTop: 15,
  },
  listItem: {
    margin: 10,
    padding: 10,
    backgroundColor: '#FFF',
    width: '80%',
    flex: 1,
    alignSelf: 'center',
    flexDirection: 'row',
    borderRadius: 5,
  },
});
