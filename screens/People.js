import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { Container, Content, Card, CardItem, Body, Text } from 'native-base';
import { TouchableOpacity, Image } from 'react-native';

function Item({ item }) {
    return (
        <Container>
        <Content>
          <Card style={{marginTop: 20}}>
            <CardItem>
              <Body>
              <Image
                source={{
                  uri:
                    'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/121360301_773769253184601_3889317023053852003_n.jpg?_nc_cat=102&_nc_sid=09cbfe&_nc_ohc=sb0Gw7zG8mIAX875dBD&_nc_ht=scontent.fpnh7-1.fna&oh=74d2d4d16c39c4458b5a8b5c3b3d8058&oe=5FB2C700',
                }}
                style={styles.profileImg}
              />
                <Text>
                   {item.title}
                </Text>
              </Body>
              <Body>
                  <TouchableOpacity
                    onPress={()=> this._onPress()}
                    style={{width: 100, marginLeft: 85 ,height: 35,borderRadius: 5 ,backgroundColor: btnBg, justifyContent: 'center', borderColor: '#ccc', borderWidth: 2}}>
                      <Text style={{textAlign: 'center', fontSize: 16}}>{textValue}</Text>
                  </TouchableOpacity>
              </Body>
            </CardItem>
          </Card>
          </Content>
          </Container>
    );
  }

class People extends Component {
    state = {
        toogle: true,
        toogle1: true,
        toogle2: true,
        toogle3: true,

        data: [
            {
                id:1,
                title: 'Animate'
            },
            {
                id:2,
                title: 'Game'
            },
            {
                id:3,
                title: 'Joke'
            },
            {
                id:4,
                title: 'Hobby'
            },
        ]
    };

  _onPress(){
      const newState = !this.state.toogle;
      this.setState({
          toogle: newState
      })
  }
  _onPress1(){
      const newState = !this.state.toogle1;
      this.setState({
          toogle1: newState
      })
  }
  _onPress2(){
    const newState = !this.state.toogle2;
    this.setState({
        toogle2: newState
    })
  }
  _onPress3(){
    const newState = !this.state.toogle3;
    this.setState({
        toogle3: newState
    })
}

  render() {

    const {toogle, toogle1, toogle2, toogle3} = this.state;
    const textValue = toogle ? "Follow":"Following";
    const btnBg = toogle ? "#28df99":"#f4f4f4";

    const textValue1 = toogle1 ? "Follow":"Following";
    const btnBg1 = toogle1 ? "#28df99":"#f4f4f4";

    const textValue2 = toogle2 ? "Follow":"Following";
    const btnBg2 = toogle2 ? "#28df99":"#f4f4f4";

    const textValue3 = toogle3 ? "Follow":"Following";
    const btnBg3 = toogle3 ? "#28df99":"#f4f4f4";

    return (
        <Container>
        <Content>
          <Card style={{marginTop: 20}}>
            <CardItem>
              <Body style={{flexDirection: 'row', flex: 1}}>
              <Image source={{uri:"https://github.githubassets.com/images/modules/open_graph/github-mark.png"}} style={{width:60, height:60,borderRadius:30}} />
                <Text style={{marginTop: 15, margin: 15}}>
                   BlackPink
                </Text>
              </Body>
              <Body>
                  <TouchableOpacity
                    onPress={()=> this._onPress()}
                    style={{width: 100, marginTop: 12 ,marginLeft: 75 ,height: 35,borderRadius: 5 ,backgroundColor: btnBg, justifyContent: 'center', borderColor: '#ccc', borderWidth: 2}}>
                      <Text style={{textAlign: 'center', fontSize: 16}}>{textValue}</Text>
                  </TouchableOpacity>
              </Body>
            </CardItem>
          </Card>
          <Card style={{marginTop: 5}}>
            <CardItem>
              <Body style={{flexDirection: 'row', flex: 1}}>
              <Image source={{uri:"https://github.githubassets.com/images/modules/open_graph/github-mark.png"}} style={{width:60, height:60,borderRadius:30}} />
                <Text style={{marginTop: 15, margin: 15}}>
                   Rose Siyan
                </Text>
              </Body>
              <Body>
                  <TouchableOpacity
                    onPress={()=> this._onPress1()}
                    style={{width: 100,marginTop: 12, marginLeft: 75 ,height: 35,borderRadius: 5 ,backgroundColor: btnBg1, justifyContent: 'center', borderColor: '#ccc', borderWidth: 2}}>
                      <Text style={{textAlign: 'center', fontSize: 16}}>{textValue1}</Text>
                  </TouchableOpacity>
              </Body>
            </CardItem>
          </Card>
          <Card style={{marginTop: 5}}>
            <CardItem>
              <Body style={{flexDirection: 'row', flex: 1}}>
              <Image source={{uri:"https://github.githubassets.com/images/modules/open_graph/github-mark.png"}} style={{width:60, height:60,borderRadius:30}} />
                <Text style={{marginTop: 15, margin: 15}}>
                   Korosina
                </Text>
              </Body>
              <Body>
                  <TouchableOpacity
                    onPress={()=> this._onPress2()}
                    style={{width: 100,marginTop: 12, marginLeft: 75 ,height: 35,borderRadius: 5 ,backgroundColor: btnBg2, justifyContent: 'center', borderColor: '#ccc', borderWidth: 2}}>
                      <Text style={{textAlign: 'center', fontSize: 16}}>{textValue2}</Text>
                  </TouchableOpacity>
              </Body>
            </CardItem>
          </Card>
          <Card style={{marginTop: 5}}>
            <CardItem>
              <Body style={{flexDirection: 'row', flex: 1}}>
              <Image source={{uri:"https://github.githubassets.com/images/modules/open_graph/github-mark.png"}} style={{width:60, height:60,borderRadius:30}} />
                <Text style={{marginTop: 15, margin: 15}}>
                   Animate RoRory
                </Text>
              </Body>
              <Body>
                  <TouchableOpacity
                    onPress={()=> this._onPress3()}
                    style={{width: 100,marginTop: 12, marginLeft: 75 ,height: 35,borderRadius: 5 ,backgroundColor: btnBg3, justifyContent: 'center', borderColor: '#ccc', borderWidth: 2}}>
                      <Text style={{textAlign: 'center', fontSize: 16}}>{textValue3}</Text>
                  </TouchableOpacity>
              </Body>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
    profileImg: {
        height: 48,
        width: 48,
        borderRadius: 40,
    },
})

export default People;
