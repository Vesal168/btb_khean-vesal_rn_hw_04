import React, { Component } from 'react';
import { View } from 'react-native';
import { Container, Header, Content, Card, CardItem, Body, Text } from 'native-base';
import { TouchableOpacity } from 'react-native';

function Item({ item }) {
    return (
        <Container>
        <Content>
          <Card style={{marginTop: 20}}>
            <CardItem>
              <Body>
                <Text>
                   {item.title}
                </Text>
              </Body>
              <Body>
                  <TouchableOpacity
                    onPress={()=> this._onPress()}
                    style={{width: 100, marginLeft: 80 ,height: 35,borderRadius: 5 ,backgroundColor: btnBg, justifyContent: 'center', borderColor: '#ccc', borderWidth: 2}}>
                      <Text style={{textAlign: 'center', fontSize: 16}}>{textValue}</Text>
                  </TouchableOpacity>
              </Body>
            </CardItem>
          </Card>
          </Content>
          </Container>
    );
  }

class Topic extends Component {
    state = {
        toogle: true,
        toogle1: true,
        toogle2: true,
        toogle3: true,

        data: [
            {
                id:1,
                title: 'Animate'
            },
            {
                id:2,
                title: 'Game'
            },
            {
                id:3,
                title: 'Joke'
            },
            {
                id:4,
                title: 'Hobby'
            },
        ]
    };

  _onPress(){
      const newState = !this.state.toogle;
      this.setState({
          toogle: newState
      })
  }
  _onPress1(){
      const newState = !this.state.toogle1;
      this.setState({
          toogle1: newState
      })
  }
  _onPress2(){
    const newState = !this.state.toogle2;
    this.setState({
        toogle2: newState
    })
  }
  _onPress3(){
    const newState = !this.state.toogle3;
    this.setState({
        toogle3: newState
    })
}

  render() {

    const {toogle, toogle1, toogle2, toogle3} = this.state;
    const textValue = toogle ? "Follow":"Following";
    const btnBg = toogle ? "#28df99":"#f4f4f4";

    const textValue1 = toogle1 ? "Follow":"Following";
    const btnBg1 = toogle1 ? "#28df99":"#f4f4f4";

    const textValue2 = toogle2 ? "Follow":"Following";
    const btnBg2 = toogle2 ? "#28df99":"#f4f4f4";

    const textValue3 = toogle3 ? "Follow":"Following";
    const btnBg3 = toogle3 ? "#28df99":"#f4f4f4";

    return (
        <Container>
        <Content>
          <Card style={{marginTop: 20}}>
            <CardItem>
              <Body>
                <Text>
                   Game
                </Text>
              </Body>
              <Body>
                  <TouchableOpacity
                    onPress={()=> this._onPress()}
                    style={{width: 100, marginLeft: 75 ,height: 35,borderRadius: 5 ,backgroundColor: btnBg, justifyContent: 'center', borderColor: '#ccc', borderWidth: 2}}>
                      <Text style={{textAlign: 'center', fontSize: 16}}>{textValue}</Text>
                  </TouchableOpacity>
              </Body>
            </CardItem>
          </Card>
          <Card style={{marginTop: 5}}>
            <CardItem>
              <Body>
                <Text>
                   Movie
                </Text>
              </Body>
              <Body>
                  <TouchableOpacity
                    onPress={()=> this._onPress1()}
                    style={{width: 100, marginLeft: 75 ,height: 35,borderRadius: 5 ,backgroundColor: btnBg1, justifyContent: 'center', borderColor: '#ccc', borderWidth: 2}}>
                      <Text style={{textAlign: 'center', fontSize: 16}}>{textValue1}</Text>
                  </TouchableOpacity>
              </Body>
            </CardItem>
          </Card>
          <Card style={{marginTop: 5}}>
            <CardItem>
              <Body>
                <Text>
                   Joke
                </Text>
              </Body>
              <Body>
                  <TouchableOpacity
                    onPress={()=> this._onPress2()}
                    style={{width: 100, marginLeft: 75 ,height: 35,borderRadius: 5 ,backgroundColor: btnBg2, justifyContent: 'center', borderColor: '#ccc', borderWidth: 2}}>
                      <Text style={{textAlign: 'center', fontSize: 16}}>{textValue2}</Text>
                  </TouchableOpacity>
              </Body>
            </CardItem>
          </Card>
          <Card style={{marginTop: 5}}>
            <CardItem>
              <Body>
                <Text>
                   Animate
                </Text>
              </Body>
              <Body>
                  <TouchableOpacity
                    onPress={()=> this._onPress3()}
                    style={{width: 100, marginLeft: 75 ,height: 35,borderRadius: 5 ,backgroundColor: btnBg3, justifyContent: 'center', borderColor: '#ccc', borderWidth: 2}}>
                      <Text style={{textAlign: 'center', fontSize: 16}}>{textValue3}</Text>
                  </TouchableOpacity>
              </Body>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}

export default Topic;
