import React from 'react';
import { StyleSheet, Text, View, FlatList, Image, TouchableOpacity } from 'react-native';
import { ActionSheet }from 'native-base'

var BUTTONS = ["Option 0", "Option 1", "Option 2", "Delete", "Cancel"];
var DESTRUCTIVE_INDEX = 3;
var CANCEL_INDEX = 4;

function Item({ item }) {
  return (
    <View style={styles.listItem}>
      <Image source={{uri:item.photo}} style={{width:60, height:60,borderRadius:30}} />
      <View style={{alignItems:"center",flex:1}}>
        <Text style={{fontWeight:"bold"}}>{item.name}</Text>
        <Text>{item.position}</Text>
      </View>
      <TouchableOpacity style={{height:50,width:50, justifyContent:"center",alignItems:"center"}}>
        <Text style={{color:"green"}}>More</Text>
      </TouchableOpacity>
    </View>
  );
}

class ProfileScreen extends React.Component {
  state = {
    data:[
        {
            "name": "Miyah Myles",
            "email": "miyah.myles@gmail.com",
            "position": "Data Entry Clerk",
            "photo": "https://i1.wp.com/celebmix.com/wp-content/uploads/2018/12/blackpink-a-year-in-review-01.jpg?fit=758%2C379&ssl=1"
        },
        {
            "name": "June Cha",
            "email": "june.cha@gmail.com",
            "position": "Sales Manager",
            "photo": "https://i1.wp.com/celebmix.com/wp-content/uploads/2018/12/blackpink-a-year-in-review-01.jpg?fit=758%2C379&ssl=1"
        },
        {
            "name": "Iida Niskanen",
            "email": "iida.niskanen@gmail.com",
            "position": "Sales Manager",
            "photo": "https://i1.wp.com/celebmix.com/wp-content/uploads/2018/12/blackpink-a-year-in-review-01.jpg?fit=758%2C379&ssl=1"
        },
        {
            "name": "Renee Sims",
            "email": "renee.sims@gmail.com",
            "position": "Medical Assistant",
            "photo": "https://i1.wp.com/celebmix.com/wp-content/uploads/2018/12/blackpink-a-year-in-review-01.jpg?fit=758%2C379&ssl=1"
        },
        {
            "name": "Jonathan Nu\u00f1ez",
            "email": "jonathan.nu\u00f1ez@gmail.com",
            "position": "Clerical",
            "photo": "https://i1.wp.com/celebmix.com/wp-content/uploads/2018/12/blackpink-a-year-in-review-01.jpg?fit=758%2C379&ssl=1"
        },
        {
            "name": "Sasha Ho",
            "email": "sasha.ho@gmail.com",
            "position": "Administrative Assistant",
            "photo": "https://i1.wp.com/celebmix.com/wp-content/uploads/2018/12/blackpink-a-year-in-review-01.jpg?fit=758%2C379&ssl=1"
        },
        {
            "name": "Abdullah Hadley",
            "email": "abdullah.hadley@gmail.com",
            "position": "Marketing",
            "photo": "https://i1.wp.com/celebmix.com/wp-content/uploads/2018/12/blackpink-a-year-in-review-01.jpg?fit=758%2C379&ssl=1"
        },
        {
            "name": "Thomas Stock",
            "email": "thomas.stock@gmail.com",
            "position": "Product Designer",
            "photo": "https://i1.wp.com/celebmix.com/wp-content/uploads/2018/12/blackpink-a-year-in-review-01.jpg?fit=758%2C379&ssl=1"
        },
        {
            "name": "Veeti Seppanen",
            "email": "veeti.seppanen@gmail.com",
            "position": "Product Designer",
            "photo": "https://i1.wp.com/celebmix.com/wp-content/uploads/2018/12/blackpink-a-year-in-review-01.jpg?fit=758%2C379&ssl=1"
        },
        {
            "name": "Bonnie Riley",
            "email": "bonnie.riley@gmail.com",
            "position": "Marketing",
            "photo": "https://i1.wp.com/celebmix.com/wp-content/uploads/2018/12/blackpink-a-year-in-review-01.jpg?fit=758%2C379&ssl=1"
        }
    ]
  }

  delete = () => {
    ActionSheet.show(
        {
          options: BUTTONS,
          cancelButtonIndex: CANCEL_INDEX,
          destructiveButtonIndex: DESTRUCTIVE_INDEX,
          title: "Testing ActionSheet"
        },
        buttonIndex => {
          this.setState({ clicked: BUTTONS[buttonIndex] });
        }
      )
  }


  render(){
    return (
      <View style={styles.container}>
        <FlatList
          style={{flex:1}}
          data={this.state.data}
          renderItem={({ item }) => <Item item={item}/>}
          keyExtractor={item => item.email}
        />
      </View>
    );
  }
}
export default ProfileScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F7F7F7',
    marginTop:15
  },
  listItem:{
    margin:10,
    padding:10,
    backgroundColor:"#FFF",
    width:"80%",
    flex:1,
    alignSelf:"center",
    flexDirection:"row",
    borderRadius:5
  },
  cardList: {
    margin: 10,
    bottom: 2,
    alignItems: 'center'
  }
});