
import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Button,
  Image,
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import {
  ActionSheet,
  Root,
  Content,
  Container,
  Textarea,
  Form,
} from 'native-base';

export default class NewStoryScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      resourcePath: {},
      des: '',
    };
  }

  selectFile = () => {
    var options = {
      title: 'Select Image',
      customButtons: [
        {
          name: 'customOptionKey',
          title: 'Choose file from Custom Option',
        },
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, res => {
      console.log('Response = ', res);

      if (res.didCancel) {
        console.log('User cancelled image picker');
      } else if (res.error) {
        console.log('ImagePicker Error: ', res.error);
      } else if (res.customButton) {
        console.log('User tapped custom button: ', res.customButton);
        alert(res.customButton);
      } else {
        let source = res;
        this.setState({
          resourcePath: source,
        });
      }
    });
  };

  render() {
    
    const {navigate} = this.props.navigation;
    let {btnPost} = styles;

    return (
      <View style={styles.container}>
        <View style={styles.container}>
          <Image
            source={{uri: this.state.resourcePath.uri}}
            style={{
              backgroundColor: '#eee',
              marginTop: 0,
              height: 200,
              width: 300,
              bottom: 0,
            }}
          />

          <TouchableOpacity onPress={this.selectFile} style={styles.button}>
            <Text style={styles.buttonText}>Select File</Text>
          </TouchableOpacity>
          <Container>
            <View>
              <Form>
                <Textarea
                  value={this.state.des}
                  onChangeText={des => this.setState({des})}
                  rowSpan={5}
                  bordered
                  placeholder="Input Description Here"
                  style={{width: 300}}
                />
              </Form>
              <TouchableOpacity style={btnPost}>
                <View>
                  <Button
                    title="Draft"
                    onPress={() =>
                      this.props.navigation.navigate('Draft', {
                        des: this.state.des,
                        resourcePath: this.state.resourcePath.uri
                      })
                    }
                  />
                </View>
                <View style={{paddingLeft: 20}}>
                  <Button title="Public"
                    onPress={() =>
                      this.props.navigation.navigate('Public', {
                        des: this.state.des,
                        resourcePath: this.state.resourcePath.uri
                      })
                    }
                   />
                </View>
              </TouchableOpacity>
            </View>
          </Container>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  button: {
    width: 180,
    height: 45,
    backgroundColor: '#3740ff',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
    marginBottom: 20,
    marginRight: 120,
    marginTop: 20,
  },
  buttonText: {
    textAlign: 'center',
    fontSize: 15,
    color: '#fff',
  },
  btnPost: {
    backgroundColor: '#fff',
    height: 50,
    width: 145,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
});
