import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Text, Content, Tab, Tabs  } from 'native-base';
import Topic from './Topic';
import People from './People';
import BeingMember from './BeingMember';

const BookmarkScreen = ({navigation}) => {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Text onPress={() => navigation.goBack()} style={{fontSize: 12}}>Back</Text>
            </Button>
          </Left>
          <Body>
            <Title>Interest</Title>
          </Body>
          <Right>
            <Button transparent>
              <Text>Cancel</Text>
            </Button>
          </Right>
        </Header>
        <Container>
        <Tabs>
          <Tab heading="Topic">
            <Topic/>
          </Tab>
          <Tab heading="People">
            <People/>
          </Tab>
          <Tab heading="Publicing">
          </Tab>
        </Tabs>
      </Container>
      </Container>
      
    );
};

export default BookmarkScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center'
  },
});
