import React, { useState } from 'react';
import {
  StyleSheet,
  Image,
  ScrollView,
  View,
  SafeAreaView,
  TouchableHighlight,
  TouchableOpacity,
  Text,
  Animated
} from 'react-native';
import ImageBlurLoading from 'react-native-image-blur-loading'
import ActionButton from '../screens/ActionButton';



class ListPost extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          profile:
            'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/53506241_402854240276106_1649826062290386944_o.jpg?_nc_cat=104&_nc_sid=174925&_nc_ohc=mQaGYL1do9QAX_TaumK&_nc_ht=scontent.fpnh7-1.fna&oh=0f23aec04179b0c95f85885edb70ea6b&oe=5FB0F839',
          des: 'ឱកាសទទួលបានអាហារូបករណ៍ ផ្នែកព័ត៌មានវិទ្យា ៧០ កន្លែង ពីមជ្ឈមណ្ឌលកូរ៉េ សហ្វវែរ អេច អ ឌី ឱកាសទទួលបានអាហារូបករណ៍ ផ្នែកព័ត៌មានវិទ្យា ៧០ កន្លែង ពីមជ្ឈមណ្ឌលកូរ៉េ សហ្វវែរ អេច អ ឌី',
          image: 'https://scontent.fpnh8-1.fna.fbcdn.net/v/t1.0-9/121640167_3654196171291510_8012836336859357255_o.jpg?_nc_cat=105&_nc_sid=730e14&_nc_eui2=AeF320tR8jS7Kgxzt8MogNj8VyGjyhfFKIJXIaPKF8Uogkj8Ndo7yoPqvfmq-LnRVGkvRm4JOxTRpnYmSa2cEAkC&_nc_ohc=ydpZyZ-LJY8AX_LeBrK&_nc_ht=scontent.fpnh8-1.fna&oh=8625d5133ea0a6ee67b8d19c59a3c8cf&oe=5FB11F89',
          name: 'Khean Visal',
        },
        {
          profile:
            'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/94891776_653111495250378_3100815015884619776_n.jpg?_nc_cat=110&_nc_sid=19026a&_nc_ohc=G-v1keU2kIYAX9hbx38&_nc_oc=AQn3CsNaBh3w7KQYWltQb4lxNo8h11ag2Rke98NcFagTcS8WqnfVYNu5qSmFg-rkzb4&_nc_ht=scontent.fpnh7-1.fna&oh=10d5357e9c4fe640157d5ff87dfb2606&oe=5FB15948',
          des: 'Tomorrow exam hehe',
          image: 'https://www.humanesociety.org/sites/default/files/styles/1240x698/public/2018/08/kitten-440379.jpg?h=c8d00152&itok=1fdekAh2',
          name: 'Koko',
        },
        {
          profile:
            'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/93323739_2610231672628715_6829459988797194240_n.jpg?_nc_cat=105&_nc_sid=a4a2d7&_nc_ohc=LUVCr3r8tGkAX8zifQ3&_nc_ht=scontent.fpnh7-1.fna&oh=c8a8345a7f8c63678b86e8937f3dcdab&oe=5FB1CEA5',
          des: 'Exam comming soon',
          image: 'https://qa-platforms.com/wp-content/uploads/2020/07/2-7.jpg',
          name: 'Dara Kong',
        },
        {
          profile:
            'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/120562856_3332635240188944_4042456883242221627_o.jpg?_nc_cat=100&_nc_sid=09cbfe&_nc_ohc=lu1CcqLf2xMAX-gdSYI&_nc_ht=scontent.fpnh7-1.fna&oh=8734bb792af6704088b27c1e55b7b334&oe=5FB09200',
          des: 'Homework nv jrern nas',
          image: 'https://pmcvariety.files.wordpress.com/2020/10/blackpink.jpg',
          name: 'Marony',
        },
        {
          profile:
            'https://nedap.com/wp-content/uploads/2020/07/Tech-Retail-2-scaled.jpg',
          des: 'Today is presentation',
          image: 'https://qa-platforms.com/wp-content/uploads/2020/07/2-7.jpg',
          mane: 'Doung Daro',
        },
        {
          profile:
            'https://nedap.com/wp-content/uploads/2020/07/Tech-Retail-2-scaled.jpg',
          des: 'Code error tt hz',
          image: 'https://qa-platforms.com/wp-content/uploads/2020/07/2-7.jpg',
          name: 'Khean Piseth',
        },
        {
          profile:
            'https://scontent.fpnh7-1.fna.fbcdn.net/v/t1.0-9/120562856_3332635240188944_4042456883242221627_o.jpg?_nc_cat=100&_nc_sid=09cbfe&_nc_ohc=lu1CcqLf2xMAX-gdSYI&_nc_ht=scontent.fpnh7-1.fna&oh=8734bb792af6704088b27c1e55b7b334&oe=5FB09200',
          des: 'Beautiful line of code',
          image: 'https://qa-platforms.com/wp-content/uploads/2020/07/2-7.jpg',
          name: 'Bro KH',
        },
      ],
    };
  }
  


  fadeInImage() {
    Animated.timing(opacity, {
      toValue: 1,
      duration: 100,
      useNativeDriver: true
    }).start()
  }

  fadeOutImage() {
    Animated.timing(opacity, {
      toValue: 0,
      duration: 1000,
      useNativeDriver: true
    }).start()
  }

  onPressBtn = () => {
    alert(this.state.data[0].des);
  };

  seeMore = () => {
    alert(this.state.data[1].des);
  }

  render() {
    return (
      <View style={styles.container_post}>
          {/* one row */}
        <View style={{flex: 1, flexDirection: 'row', marginTop: 12}}>
          <View style={{width: 100, height: 50}}>
            <TouchableHighlight
              style={[
                styles.profileImgContainer,
                {borderColor: 'green', borderWidth: 1},
              ]}>
              <Image
                source={{
                  uri: this.state.data[0].profile,
                }}
                style={styles.profileImg}
              />
            </TouchableHighlight>
          </View>
          <View
            style={{
              width: 50,
              flex: 2,
              height: 50,
            }}>
            <TouchableOpacity
              style={styles.loginScreenButton}
              underlayColor="#fff">
              <Text style={styles.loginText}>{this.state.data[0].name}</Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              width: 50,
              flex: 0,
              height: 50,
            }}>
            <TouchableOpacity style={styles.seemore} underlayColor="#fff" onPress={this.onPressBtn.bind(this)}>
              <Text style={styles.doticon}>...</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{flex: 1, flexDirection: 'row', marginTop: 15}}>
          <View style={{width: '100%', height: 50}}>
            <Text style={styles.desc} numberOfLines={2} ellipsizeMode="tail" >{this.state.data[0].des}</Text>
          </View>
        </View>
        <View style={{flex: 1, flexDirection: 'row', marginTop: -10}}>
          <View style={{width: '100%', height: 200}}>
            <TouchableHighlight style={styles.borderStyle}>

              <Animated.View>
              <ImageBlurLoading withIndicator
                source={{
                  uri: this.state.data[0].image,
                }}
                style={styles.imageDES}
              />
              </Animated.View>

            </TouchableHighlight>
          </View>
        </View>
        <View>
          <ActionButton/>
        </View>
      {/* two row */}
      <View style={{flex: 1, flexDirection: 'row', marginTop: 12}}>
          <View style={{width: 100, height: 50}}>
            <TouchableHighlight
              style={[
                styles.profileImgContainer,
                {borderColor: 'green', borderWidth: 1},
              ]}>
              <Image
                source={{
                  uri: this.state.data[3].profile,
                }}
                style={styles.profileImg}
              />
            </TouchableHighlight>
          </View>
          <View
            style={{
              width: 50,
              flex: 2,
              height: 50,
            }}>
            <TouchableOpacity
              style={styles.loginScreenButton}
              underlayColor="#fff">
              <Text style={styles.loginText}>{this.state.data[3].name}</Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              width: 50,
              flex: 0,
              height: 50,
            }}>
            <TouchableOpacity style={styles.seemore} underlayColor="#fff" onPress={this.onPressBtn.bind(this)}>
              <Text style={styles.doticon}>...</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{flex: 1, flexDirection: 'row', marginTop: 15}}>
          <View style={{width: '100%', height: 50}}>
            <Text style={styles.desc} numberOfLines={2} ellipsizeMode="tail" >{this.state.data[3].des}</Text>
          </View>
        </View>
        <View style={{flex: 1, flexDirection: 'row', marginTop: -10}}>
          <View style={{width: '100%', height: 200}}>
            <TouchableHighlight style={styles.borderStyle}>
              <ImageBlurLoading withIndicator
                source={{
                  uri: this.state.data[3].image,
                }}
                style={styles.imageDES}
              />
            </TouchableHighlight>
          </View>
        </View>
        <View>
          <ActionButton/>
        </View>


        {/* three row */}
        <View style={{flex: 1, flexDirection: 'row', marginTop: 12}}>
          <View style={{width: 100, height: 50}}>
            <TouchableHighlight
              style={[
                styles.profileImgContainer,
                {borderColor: 'green', borderWidth: 1},
              ]}>
              <Image
                source={{
                  uri: this.state.data[1].profile,
                }}
                style={styles.profileImg}
              />
            </TouchableHighlight>
          </View>
          <View
            style={{
              width: 50,
              flex: 2,
              height: 50,
            }}>
            <TouchableOpacity
              style={styles.loginScreenButton}
              underlayColor="#fff">
              <Text style={styles.loginText}>{this.state.data[1].name}</Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              width: 50,
              flex: 0,
              height: 50,
            }}>
            <TouchableOpacity style={styles.seemore} underlayColor="#fff" onPress={this.seeMore.bind(this)}>
              <Text style={styles.doticon}>...</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{flex: 1, flexDirection: 'row', marginTop: 15}}>
          <View style={{width: '100%', height: 50}}>
            <Text style={styles.desc}>{this.state.data[1].des}</Text>
          </View>
        </View>
        <View style={{flex: 1, flexDirection: 'row', marginTop: -10}}>
          <View style={{width: '100%', height: 200}}>
            <TouchableHighlight style={styles.borderStyle}>
              <ImageBlurLoading withIndicator
                source={{
                  uri: this.state.data[1].image,
                }}
                style={styles.imageDES}
              />
            </TouchableHighlight>
          </View>
        </View>
        <View>
          <ActionButton/>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container_post: {
    flex: 1,
    width: '92%',
    marginRight: 10,
    marginLeft: 18,
  },
  seemore: {
    paddingBottom: 20,
  },
  desc: {
    fontSize: 15,
    marginTop: 5,
    bottom: 12,
  },
  borderStyle: {
    borderRadius: 10,
  },
  imageDES: {
    width: '100%',
    height: '100%',
  },
  doticon: {
    marginTop: 15,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  loginScreenButton: {
    marginRight: 20,
    paddingTop: 10,
    paddingBottom: 5,
    borderRadius: 40,
    borderWidth: 1,
    borderColor: '#fff',
    width: '50%',
  },
  profileImgContainer: {
    marginLeft: 5,
    height: 50,
    width: 50,
    borderRadius: 40,
  },
  loginText: {
    color: '#000',
    textAlign: 'left',
    paddingLeft: 10,
    paddingRight: 10,
    marginLeft: -40,
    padding: 8,
  },
  profileImg: {
    height: 48,
    width: 48,
    borderRadius: 40,
  },
});

export default ListPost;
